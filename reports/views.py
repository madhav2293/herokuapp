from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
import django.db
from django.conf import settings
import json
from django.contrib.auth.decorators import login_required
from django.db import models
from django.contrib.auth.models import User
from datetime import datetime, timedelta
from django.template import Context
from django.template.loader import render_to_string
from reports.models import StudentsEducationDetails, StudentsPersonalDetails, City,University
import os
from random import randint, choice, uniform
from django.core.mail import send_mail
from reports.signals import *
from django.core.mail import EmailMultiAlternatives
from datetime import datetime
import codecs
from django.db.models.signals import post_save
from django.dispatch import receiver, Signal


@csrf_exempt
def home(request):
    return render(request, "home.html")

@csrf_exempt
def submit(request):
    return render(request, "form.html")

@csrf_exempt
def adminpanel(request):
    return render(request, "adminpanel.html")

@csrf_exempt
def generatePersonalDetails(request):
  """ This method populates the fake data for student's personal
      details
  """
  gender_choice = [0, 1]
  course_choice =  ['Engineering','MBA','Pharmacy']
  cast_choice =  ['Open','ST','SC']
  current_date = datetime.now()
  i = 1
  end_date = current_date - timedelta(days=180)
  while current_date.date()  > end_date.date() :
     va = end_date.date()
     new_obj = StudentsPersonalDetails()
     new_obj.name = 'student'+ str(i) 
     new_obj.email = 'student'+ str(i) + '@gmail.com'
     new_obj.cast = choice(cast_choice)
     new_obj.pcity_id = randint(1,3)
     new_obj.course = choice(course_choice)
     new_obj.gender =  choice(gender_choice)
     new_obj.age = randint(20,30)
     new_obj.timestamp = end_date 
     new_obj.save()
     end_date = end_date + timedelta(days=2)
     i = i + 1

  return HttpResponse("ok")

@csrf_exempt
def generateEducationalDetails(request):
  """ This method populates the fake data for student's educational
      details
  """
  gender_choice = [0, 1]
  course_choice =  ['Engineering','MBA','Pharmacy']
  cast_choice =  ['Open','ST','SC']
  current_date = datetime.now()
  i = 1
  end_date = current_date - timedelta(days=180)
  while current_date.date()  > end_date.date() :
     va = end_date.date()
     new_obj = StudentsEducationDetails()
     new_obj.student_id = i 
     new_obj.school = 'school'+ str(i) 
     new_obj.percentage = uniform(50.00, 100.00)
     new_obj.passout = randint(2008,2014)
     new_obj.ecity_id = randint(1,12)
     new_obj.uni_id = randint(1,12)
     new_obj.gap =  randint(0,4)
     new_obj.save()
     end_date = end_date + timedelta(days=2)
     i = i + 1

  return HttpResponse("ok")

@csrf_exempt
def populateCityNames(request):
  """This method populates the city names in database
  """

  cityNames =  ['Mumbai','Delhi','Chennai', 'Bangalore','Hyderabad','Ahmedabad', 'Kolkata','Pune','Surat',
        'Ludhiana','Lucknow','Visakhapatnam']
  for c in cityNames:
    new_c = City()
    new_c.name = c 
    new_c.save()
  return HttpResponse("ok")

@csrf_exempt
def populateUniversityNames(request):
  """This method populates the university names in database
  """
  universityNames =  ['Indian Institute of Technology Bombay','Indian Institute of Technology Delhi',
  'Anna University','Manipal University','Panjab University','Indian Institute of Technology Roorkee',
  'Indian Institute of Science','University of Pune','Lovely Professional University',
  'Gujarat Technological University','Birla Institute of Technology and Science','Indian Institute of Technology Gandhinagar']
  
  for u in universityNames:
    new_u = University()
    new_u.name = u 
    new_u.save()
           
  return HttpResponse("ok")

@csrf_exempt
def sendNotification(request):
      
      p = json.loads(request.body)
      
      emailid = p['email']
      name = p['name']
      appid = p['appid']
      course = p['course']
      try:
          c = {}
          c['name'] = name 
          c['Appid'] = appid
          c['course'] = course  
         
          text_content = 'student Registration'
          html_content = render_to_string('notify.html', c)
          email = EmailMultiAlternatives(text_content)
          email.attach_alternative(html_content, "text/html")
          email.to = [emailid]
          email.send()
      except:
          pass
      
      return HttpResponse('ok')