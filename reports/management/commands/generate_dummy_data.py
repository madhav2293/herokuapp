from django.shortcuts import render
from django.core.management.base import BaseCommand
from django.db import models
from django.contrib.auth.models import User
from datetime import datetime, timedelta
from django.template import Context
from django.template.loader import render_to_string
from reports.models import StudentsPDetails,StudentsEDetails
import os
from random import randint, choice, uniform


class Command(BaseCommand):
    args = ''
    help = 'Command for generating dummy data of Students'
    #**************************************#
    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)
        
    def handle(self, *args, **options):
           
           gender_choice = [0, 1]
           course_choice =  ['Engineering','MBA','Pharmacy']
           cast_choice =  ['Open','ST','SC']
           current_date = datetime.now()
           i = 1
           end_date = current_date - timedelta(days=180)
           while current_date.date()  > end_date.date() :
               va = end_date.date()
               new_obj = StudentsEDetails()
               new_obj.student_id = i 
               new_obj.school = 'school'+ str(i) 
               new_obj.percentage = uniform(50.00, 100.00)
               new_obj.passout = randint(2008,2014)
               new_obj.uni_id = randint(1,10)
               new_obj.gap =  randint(0,4)
               new_obj.save()
               end_date = end_date + timedelta(days=2)
               i = i + 1
               