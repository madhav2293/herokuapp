from django.db import models
from datetime import datetime
from django.core import serializers
import json
from django.dispatch import receiver, Signal
from django.db.models.signals import post_save
# Create your models here.

class City(models.Model):
    """ This model stores the city details and used as reference table in
        StudentsPersonalDetails
    """
    name = models.CharField(max_length = 50, null=True, blank=True)
    state = models.CharField(max_length = 50, null=True, blank=True)

class University(models.Model):
    """ This model stores the university details and used as reference table in
        StudentsEducationDetails
    """
    name = models.CharField(max_length = 50, null=True, blank=True)

class StudentsPersonalDetails(models.Model):
    """ This model defines the structure of student's
        personal details
    """
    
	# Here 0  in gender stands for female while 1  stands for male 

    name = models.CharField(max_length = 50, null=True, blank=True)
    gender = models.BooleanField()
    age  =  models.IntegerField(max_length = 3, null=True, blank=True)
    pcity = models.ForeignKey(City, related_name = "pcity" , null=True, blank=True)
    course  = models.CharField(max_length = 50, null=True, blank=True)
    cast = models.CharField(max_length = 50, null=True, blank=True)
    email = models.CharField(max_length = 50, null=True, blank=True)
    timestamp = models.DateTimeField(max_length = 50, null=True, blank=True)

class StudentsEducationDetails(models.Model):
    """ This model defines the structure of student's
        educational details
    """
    student = models.ForeignKey(StudentsPersonalDetails, related_name = "student" , null=True, blank=True)
    school = models.CharField(max_length = 50, null=True, blank=True)
    ecity = models.ForeignKey(City, related_name = "ecity" , null=True, blank=True)
    percentage  =  models.FloatField(max_length = 5, null=True, blank=True)
    passout  = models.CharField(max_length = 50, null=True, blank=True)
    uni = models.ForeignKey(University, related_name = "uni" , null=True, blank=True)
    gap = models.IntegerField(max_length = 50, null=True, blank=True)




    