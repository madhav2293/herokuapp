from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from reports.models import StudentsEducationDetails, StudentsPersonalDetails, City,University
from tastypie.authorization import Authorization
from tastypie import fields,  utils
from tastypie.constants import ALL
from tastypie.validation import Validation
from datetime import datetime, timedelta       
import json
from django.http.response import HttpResponse

       
class UniversityResource(ModelResource):
    """This api is for retriving university details
    """
    
    class Meta:
        queryset = University.objects.all()
        resource_name = 'university'
        allowed_methods = ['get', 'patch', 'post', 'delete']
        filtering = {
            'name' : ALL,
        }
        authorization= Authorization()
        always_return_data = True
 
class CityResource(ModelResource):
    """This api is for retriving city details
    """
    
    class Meta:
        queryset = City.objects.all()
        resource_name = 'city'
        allowed_methods = ['get', 'patch', 'post', 'delete']
        filtering = {
            'name' : ALL,
            'state' : ALL,
        }
        authorization= Authorization()
        always_return_data = True

class StudentPResource(ModelResource):
    """This api is for retriving student's personal details
    """
    pcity = fields.ForeignKey(CityResource, 'pcity', blank=True, null=True, full=True)
    class Meta:
        queryset = StudentsPersonalDetails.objects.all()
        resource_name = 'studentPersonalDetails'
        allowed_methods = ['get', 'patch', 'post', 'delete']
        filtering = {
            'gender' : ALL,
            'course' : ALL,
            'pcity' : ALL,
            'age' : ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'cast' : ALL,
            'email' : ALL,
        }
        authorization= Authorization()
        always_return_data = True

    def hydrate(self, bundle, request=None):
        bundle.obj.timestamp = datetime.now()
        return bundle


class StudentEResource(ModelResource):
    """This api is for retriving student's educational details
    """
    student = fields.ForeignKey(StudentPResource, 'student', blank=True, null=True, full=True)
    ecity = fields.ForeignKey(CityResource, 'ecity', blank=True, null=True, full=True)
    uni = fields.ForeignKey(UniversityResource, 'uni', blank=True, null=True, full=True)

    class Meta:
        queryset = StudentsEducationDetails.objects.all()
        resource_name = 'studentEducationalDetails'
        allowed_methods = ['get', 'patch', 'post', 'delete']
        filtering = {
            'student' : ALL,
            'school' : ALL,
            'ecity' : ALL,
            'percentage' : ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'passout' : ALL,
            'uni' : ALL,
            'gap' : ALL,
        }
        ordering = ['percentage']
        authorization= Authorization()
        always_return_data = True
        


